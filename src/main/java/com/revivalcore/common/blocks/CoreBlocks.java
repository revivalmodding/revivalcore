package com.revivalcore.common.blocks;

import net.minecraft.block.Block;

import java.util.ArrayList;
import java.util.List;

public class CoreBlocks {
    public static final List<Block> BLOCK_LIST = new ArrayList<Block>();

    public static final Block SUIT_MAKER = new BlockSuitMaker("suit_maker");
}
